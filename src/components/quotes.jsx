import React, {Component } from "react";

class Quotes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      fontSize: "4vmin",
      quote: [
        {
          attributes:
          {
            text: "Minimalism is not a lack of something. It’s simply the perfect amount of something."
          },
          id: 1,
          relationships: {
            author: {
              data:{
                id: 1,
                type: "author"
              }
            }
          }
        }
      ],
      author:[
        {
          attributes: {
            image: "test.jpg",
            name: "Test"
          },
          id: 1,
          type: "author"
        }
      ],
      authors: {},
      quotes: {}
    };
  }

  async componentDidMount() {
    // if first session visit & sessionstorage empty
    if (!sessionStorage.getItem("quotes")) {
      await this.fetchQuotes().then(data =>
        console.log("data fetched :", data)
      );
    }
    const sessionStorageValue = JSON.parse(sessionStorage.getItem("quotes"));
    const authorsValue = JSON.parse(sessionStorage.getItem("authors"));
    await this.setState(
      {
        quotes: sessionStorageValue,
        authors: authorsValue
      },
      () => {
        this.getNewQuote();
      }
    );
  }

  getNewQuote = () => {
    const { quotes, authors } = this.state;
    const keys = Object.keys(quotes);
    const randomIndex = keys[Math.floor(Math.random() * keys.length)];
    const item = quotes[randomIndex];
    const authorItem = authors.find((element) => {
      return element.id == item.relationships.author.data.id;
    });

    this.setState({ quote: item, author: authorItem, isLoading: false }, () => {
      const { isLoading } = this.state;
      isLoading ? null : this.fontAdjust();
    });

    return item;
  };

  async fetchQuotes() {
    const url = "https://randomstoicquotesapi.herokuapp.com/api/v1/quotes";
    const response = await fetch(url, 
      {
      headers: {
        Accept: "application/vnd.api+json"
      }
    });
    const data = await response.json();
    sessionStorage.setItem("quotes", JSON.stringify(data.data));
    sessionStorage.setItem("authors", JSON.stringify(data.included));
    return data;
  }

  fontAdjust = value => {
    const { quote } = this.state;

    const quoteWordLength = quote.attributes.text.split(" ").length;
    const longQuote = quoteWordLength > 80; // over 400 chars.. rough estimate

    if (longQuote) {
      const quoteStyle = document
        .querySelectorAll(".quotes__content p")
        .forEach(el =>
          el.setAttribute("style", "font-size: 3.2vmin; line-height: 1.8rem;")
        );
      quoteStyle;
    }
  };

  render() {
    const { quote, author, isLoading } = this.state;
    
    return (
      
      <>
      <div className = 'background'>
        <div className="quotes">
          {isLoading ? (
            <span className="quotes__content">
              <p>Please hold..</p>
            </span>
          ) : (
            <>
              <span
                className="quotes__content"
                dangerouslySetInnerHTML={{ __html: "<p>" + quote.attributes.text + "</p>"}}
              />
              <p
                className="quotes__title"
                dangerouslySetInnerHTML={{ __html: "<p>" + author.attributes.name +"</p>" }}
              />
            </>
          )}
        </div>
        <div className="footer">
          <div className="footer__content">
            <a
              title="Wannabe stoic"
              href="https://github.com/sukh0128"
              className="footer__item"
            >
              Shameless plug
            </a>
            <a
              title="What is Stoicism?"
              href="https://www.youtube.com/watch?v=R9OCA6UFE-0&ab_channel=TED-Ed"
              className="footer__item footer__item--frs"
            >
              Stoicism
            </a>
          </div>
        </div>
      </div>
      </>
    );
  }
}

export default Quotes;
