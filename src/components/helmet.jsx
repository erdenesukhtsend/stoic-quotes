const typekit = "https://use.typekit.net/qcn8ytc.css";
const featherIcons =
  "https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js";
const title = "Daily Stoic Quotes";

const Helmet = {
  title,
  htmlAttributes: { lang: "en" },
  link: [{ rel: "stylesheet", href: typekit }],
  script: [{ src: featherIcons }]
};

export default Helmet;
