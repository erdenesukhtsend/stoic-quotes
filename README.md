# Stoic Quotes Single Page Application


  [Daily Stoic Quotes Website](https://daily-stoic-quotes.web.app/) 

  Inspired by and used the Quotes object from : https://github.com/soirs/quotes-on-design

  Gets the quotes data using the API from : https://github.com/jimmerioles/random-stoic-quotes-api

## Tech Stack used:

 * ReactJS
 * GatsbyJS
 * Google Firebase for deployment
 * Bitbucket for Source Control
